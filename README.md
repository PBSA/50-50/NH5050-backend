# NH5050-Backend

## Dependencies: 

The following has been tested on the latest Ubuntu 20.04.

This installation document assumes you are running your own testnet, or have access to the current "mint" testnet.  If you do not have access to either of the above, please refer to the documentation here:

- Your own testnet running -  Instructions can be found [here for docker](https://infra.peerplays.tech/advanced-topics/private-testnets/peerplays-qa-environment)  and [here for system service!](https://infra.peerplays.tech/advanced-topics/private-testnets/private-testnets-manual-install)
- Using the testnet cli_wallet to connect to: wss://mint.peerplays.download/api - along with relevant testnet funds

You will also need to create two blockchain accounts, a payment account and an escrow account.  If you do not already have accounts you will use for these purposes, instructions for creating them are [here](/docs/accountCreation.md).

### Node.js

You will need Node.js version 12.13.0 - This can easily be installed via the NVM repo [here!](https://github.com/nvm-sh/nvm) or using the following script:


- Installing NVM:  `curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash`
- Installing/Using Node 12:  `nvm install 12 && nvm use 12` 


### Installing, and enabling system start up of Postgres v14.x 

_Note: Docker is required if you are running Postgres 12.x on the same system already._

```
sudo apt update && sudo apt dist-upgrade 
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" | sudo tee /etc/apt/sources.list.d/postgresql-pgdg.list
sudo apt update
sudo apt install postgresql-14
sudo systemctl enable postgresql.service && sudo systemctl start postgresql.service
sudo -i -u postgres
psql
CREATE DATABASE lotto;
\q
exit
```

## Configuring Postgress Backend
Cloning the backend - `git clone https://gitlab.com/PBSA/50-50/NH5050-frontend.git`

Configure the `config/default.json` file with relevant Postgres connection information 

NOTE: The commited default.json file is an EXAMPLE - Modify this using your best security practices, for security reasons, you should not using the following example file. 

Changing default ports & **not** using the default postgres user is **HIGHLY** recommended

```
DB_USER=postgres
DB_PASSWORD=123456
DB_HOST=postgres
DB_PORT=5432
DB_DATABASE=nft-lotto
```

### Email sender setup

Follow the documentation on how to add an app password, and use this inside of the following section:

https://devanswers.co/create-application-specific-password-gmail/

```
  "mailer": {
    "host": "smtp.gmail.com",
    "port": 587,
    "secure": false,
    "auth": {
      "user": "<your_email_address>",
      "pass": "<app_password_provided_by_google>"
    },
    "sender": "<your_name>",
    "tls": {
        "rejectUnauthorized": false

```
### Configuring Peerplays testnet information
```
  "frontendUrl": "<wherever_your_front_url_lives>", - this can be localhost, or the complete URL
  "backendUrl": "http://localhost:3000", - Leaving this has default is fine, reverse proxy with SSL is recommended
  "frontendCallbackUrl": "http://localhost:8082/callback",
  "peerplays": {
    "peerplaysWS": "wss://mint.peerplays.download/api", - Your testnet api or leave as default to connect to Peerplays testnet
    "peerplaysFaucetURL": "https://mint-faucet.peerplays.download/api/v1/accounts", - Your faucet URL, this would be attached to testnet setup, or leave default to use the mint-testnet
    "referrer": "1.2.0", - This is the commitee account on general testnets
    "sendAssetId": "1.3.0", - This is the on-chain asset ID for TEST token (PPY on mainnet)
    "paymentAccountID": "1.2.x", - payment account
    "paymentAccountWIF": "5HttHcgL2NgFc5XsFY8bs51VehVDS2Tb4NGkRuwjJ6v6Mq7eC7S" - payment account key
    "paymentReceiver": "1.2.1163", - escrow account
    "paymentReceiverWIF": "5HpxiGbhGgayv4YE43Jtp5R95aKCXoUiKH6eCadaVfGyRwKjLPu", - escrow account key
    "ticketPrice": 1, - default ticket price
    "ticketAssetID": "1.3.0", - default ticket assetID
    "maxTicketSupply": 10000000 - default max ticket supply

```

## Stripe & S3 Config 
```
"s3": {
    "bucket": "nh5050-dev"
  },
  "cdnUrl": "http://d2auf3ow1jba87.cloudfront.net",
  "stripe": {
    "publishableKey": "pk_test_eXMu4Pj53sjl7Ff2pj3xYPh8",
    "secretKey": "sk_test_vC4hHCnGd6c2q7l4XZZEtDV0",
    "endpointSecret": "whsec_gqPI1tTE8B8JzNLtUlUOCB2hZnmoSwFt"
  }
```

## Serving the application

You can use the following commands to install the dependencies & run the software from the root directory of the project

```
npm i
npm run start 
```

It is recommended to use pm2 to serve this application in the background, this can be done like the following:

- `npm install pm2 -g` - installing globally
- `pm2 --name peerid-backend start npm -- start`
- `pm2 logs peerid-backend` - Checking logs to ensure startup was successful

Here are a few other pm2 commands that can be useful: 

`pm2 restart peerid-backend` - Restarting the service after changes
`pm2 stop peerid-backend` - Gracefully stopping the service
`pm2 del peerid-backend` - Gracefully deleting application

It is also best practice to use a reverse proxy with TLS/SSL - Here is an example using nginx:

```
server {
  server_name <your URL/DOMAIN>;
  root /var/www/peerid-gui;

   add_header 'Access-Control-Allow-Origin' '*' always;
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
   add_header 'Access-Control-Allow-Headers' 'X-Requested-With,Accept,Content-Type,Authorization, Origin' always;

  location /api {
   add_header 'Access-Control-Allow-Origin' '*' always;
   add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
   add_header 'Access-Control-Allow-Headers' 'X-Requested-With,Accept,Content-Type,Authorization, Origin' always;

  proxy_pass http://localhost:3000;

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
  }

}
```

## API Documentation

You can find API documentation [here!](/docs/swagger.yaml)



## How to force migration of databases (needed for first install)

To run all pending migrations
```npm run db-migrate-all```

To undo single migrations
```npm run db-migrate-undo```

To undo all migrations BE CAREFUL
```npm run db-migrate-undo```

To run all Seeds. Seeds can be run multiple times and should be used for dev only
```npm run db-seed-all```

To undo single migrations
```npm run db-seed-undo-all```



## Quick Instalation Instructions

1. install node packages using `npm i`
2. update config `./config/default.json` file with postgresql and peerplays info
3. set up postgresql run `npm run db-migrate-all & npm run db-seed-all`
4. using the postgresql command line delete dummy sales and entries using `delete from entries;` & `delete from sales;`
5. run `npm run start`